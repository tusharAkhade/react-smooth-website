import React from "react";
import ScrollToTop from "../components/SignIn/ScrollToTop";
import SignIn from "../components/SignIn/SignIn";

const SignInPage = () => {
  return (
    <>
      <ScrollToTop />
      <SignIn />
    </>
  );
};

export default SignInPage;
